#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from rest_framework import serializers
from snippets.models import Snippet

class SnippetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.Field(source='owner.username')
    highlight = serializers.HyperlinkedIdentityField(view_name='snippet-highlight', format='html')
    # source argument controls which attr is used to populate the field,
    # and can point at any attr in the serializer instance.
    # Untyped `Field` is always read-only, only used for serialized
    # representations, not for updating models when they are deserialized
    class Meta:
        model = Snippet
        fields = ('url', 'highlight', 'title', 'code', 'linenos', 'language', 'style', 'owner')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    # Snippets will not be included by default when using the ModelSerializer
    # class, it's a "reverse" relationship on the User model
    snippets = serializers.HyperlinkedRelatedField(many=True, view_name='snippet-detail')
    class Meta:
        model = User
        fields = ('url', 'username', 'snippets')
