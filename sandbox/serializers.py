#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .models import Thing, PeerThing
from rest_framework import serializers

class ThingSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Thing
        fields = ('aField', 'aCommonField')

class PeerThingSerializer(serializers.HyperlinkedModelSerializer):
    real_thing = serializers.RelatedField()

    class Meta:
        model = Thing
        fields = ('aField', 'aCommonField', 'real_thing')

