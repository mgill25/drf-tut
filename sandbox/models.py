from django.db import models

# Create your models here.

class Thing(models.Model):
    aField = models.CharField(max_length=100, default='ThingAField')
    aCommonField = models.CharField(max_length=100, default='ThingCommonAField')

    name = models.CharField(max_length=100)

    def __unicode__(self):
        return '%s' % self.name

class PeerThing(models.Model):
    aField = models.CharField(max_length=100, default='PeerAField')
    aCommonField = models.CharField(max_length=100, default='PeerThingCommonAField')
    real_thing = models.OneToOneField(Thing)
    name = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        """
        Establish the relationship b/w Thing and PeerThing based on the
        'name' field.
        """
        if self.pk is None:
            thing, created = Thing.objects.get_or_create(name=self.name)
            self.real_thing = thing
            super(PeerThing, self).save(*args, **kwargs)
        else:
            super(PeerThing, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
