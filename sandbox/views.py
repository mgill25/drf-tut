# Create your views here.
from .models import Thing, PeerThing
from rest_framework import viewsets
from .serializers import ThingSerializer, PeerThingSerializer

class ThingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Thing.objects.all()
    serializer_class = ThingSerializer

class PeerThingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = PeerThing.objects.all()
    serializer_class = PeerThingSerializer

