#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

from middle.models import Thing as MiddleThing

# Interface inside "Front", which commmunicates with Middle

class PeerThing(models.Model):

    class Meta:
        abstract = True

    aField = models.CharField(max_length=100, default='PeerAField')
    aCommonField = models.CharField(max_length=100, default='PeerThingCommonAField')
    real_thing = models.OneToOneField(MiddleThing)
    name = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        if self.pk is None:
            thing, created = Thing.objects.get_or_create(name=self.name)
            self.real_thing = thing
            super(PeerThing, self).save(*args, **kwargs)
        else:
            super(PeerThing, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
