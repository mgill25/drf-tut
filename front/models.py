from django.db import models


# Front --> Client
# Create your models here.
class Thing(models.Model):
    aField = models.CharField(max_length=100, default='ThingAField')
    aCommonField = models.CharField(max_length=100, default='ThingCommonAField')

    name = models.CharField(max_length=100)

    def __unicode__(self):
        return '%s' % self.name


