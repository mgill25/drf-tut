#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

from back.models import Thing as BackThing

# Interface inside "Middle", which transfers model representations
# b/w Front and Back.

class PeerThing(models.Model):

    class Meta:
        abstract = True

    aField = models.CharField(max_length=100, default='PeerAField')
    aCommonField = models.CharField(max_length=100, default='PeerThingCommonAField')
    real_thing = models.OneToOneField(BackThing)
    name = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        if self.pk is None:
            thing, created = Thing.objects.get_or_create(name=self.name)
            self.real_thing = thing
            super(PeerThing, self).save(*args, **kwargs)
        else:
            super(PeerThing, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name
